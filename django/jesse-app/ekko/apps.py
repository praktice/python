from django.apps import AppConfig


class EkkoConfig(AppConfig):
    name = 'ekko'
