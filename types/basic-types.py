from types import *


a: int = 1
b: float = 2.2

c: str = 'Hello'
d: List = List[1, 2, 3]
e: Tuple = ('uni', 'que')
f: Vector = List[float]  # Same as List
g: bytes b"ttt"
h: Set[int] = {1, 1, 2}


"""
Union
Any
object
Deque
Optional
ClassVar
Set
FrozenSet
Awaitable
Coroutine
AsyncIterable
AsyncIterator
ContextManager
AsyncContxtManager
Dict
Text
IO
TextIO
BinaryIO
Pattern
Match
@typing.overload
NoReturn
""""

# Custom Types
my_type = NewType('user_id', int)
user_id = my_type(200)

# Callbacks
def pass_cb(next: Callable([], str]) -> None:
  pass

# Generics
# ! Can pass params by using TypeVar factory
my_map = Mapping[str, str]
my_seq = Sequences[Unsure]

T = TypeVar('T')
def genFunc(x: Sequence[T]) -> T:
  return x[0]

# Iterable
data = Iterable[1,2,3,4,5]
def nothing(data: Iterable[list[int]] -> None:
  for d in data:
    d.set(0)

# function with arg type, return type
def hello(name: str) -> str:
  return 'Morning ' + name

# Get the Type
type(a)
